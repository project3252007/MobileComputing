using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prendi : MonoBehaviour
{

    public GameObject Bprendi;
    // Start is called before the first frame update
    void Start()
    {
        Bprendi.SetActive(false);
    }
void OnTriggerStay(Collider other)
    {
       
        if (other.gameObject.name == "Giocatore") 
        {
           Bprendi.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
       
        if (other.gameObject.name == "Giocatore") 
        {
           Bprendi.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
