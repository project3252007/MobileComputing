using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class CambioScena2 : MonoBehaviour
{
  public GameObject loadingPanel;

     void OnTriggerEnter(Collider other){
    StartCoroutine(LoadScene("pianeta"));
    }

    IEnumerator LoadScene(string sceneName){
    loadingPanel.SetActive(true);
     AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);

     while(!operation.isDone){
        yield return null;
     }
    }
}


