using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrendiPalla : MonoBehaviour
{

     public Transform PlayerTransform;
    public GameObject cell;
    public Camera Camera;
    public float range = 2f;
    public float open = 100f;
    // Start is called before the first frame update
   void Start()
    {
        cell.GetComponent<Rigidbody>().isKinematic = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("f"))
        {
            UnequipObject();
            
        }
        if (Input.GetKeyDown("e"))
        {
            
            EquipObject();
        }
    }


    void UnequipObject()
    {
        PlayerTransform.DetachChildren();
       cell.transform.eulerAngles = new Vector3(cell.transform.eulerAngles.x, cell.transform.eulerAngles.y, cell.transform.eulerAngles.z - 0);
        cell.GetComponent<Rigidbody>().isKinematic = false;
    }

    void EquipObject()
    {
        cell.GetComponent<Rigidbody>().isKinematic = true;
        cell.transform.position = PlayerTransform.transform.position;
        cell.transform.rotation = PlayerTransform.transform.rotation;
        cell.transform.SetParent(PlayerTransform);
    }


}
