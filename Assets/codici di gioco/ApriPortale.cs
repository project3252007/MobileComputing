using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApriPortale : MonoBehaviour
{
  public AudioSource source;
   

    void awake(){
        source = GetComponent<AudioSource>();
       
    }

 public GameObject cubePrefab;
 public Transform  transform;
    public GameObject p;
   
    void Start(){
        p.SetActive(false);
        
    }
    public void Aportale(){
       p.SetActive(true);
       
        Instantiate(cubePrefab, transform.position, Quaternion.identity);
        source.Play();
    }
  
}
