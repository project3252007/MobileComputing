using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Morte : MonoBehaviour
{
    


    public float scaleFactor = 20.0f; // Fattore di scala per ogni secondo
    public float maxScale = 2000.0f; // Massima dimensione a cui può crescere il GameObject

    private void Update()
    {
        // Aumenta la dimensione del GameObject
        transform.localScale += new Vector3(scaleFactor, scaleFactor, scaleFactor) * Time.deltaTime;

        // Limita la dimensione massima
        transform.localScale = Vector3.Min(transform.localScale, new Vector3(maxScale, maxScale, maxScale));
    }
}

   
   
