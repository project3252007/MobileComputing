using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneraBase3 : MonoBehaviour
{
    public GameObject objToTP;
    public Transform tpLoc;
    private Rigidbody heldObjRb;
    void OnTriggerStay(Collider other)
    {
       
        if (other.gameObject.name == "cristal") 
        {
           heldObjRb =  objToTP.GetComponent<Rigidbody>();
           heldObjRb.isKinematic = true;
            objToTP.transform.position = tpLoc.transform.position;
        }
    }
   
}
