using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchTarget : MonoBehaviour
{
    public GameObject F1;
    public GameObject F2;
    // Start is called before the first frame update
    void Start()
    {
        F1.SetActive(true);
        F2.SetActive(false);
    }
void OnTriggerEnter(Collider other)
    {
          F1.SetActive(false);
          F2.SetActive(true);
        
    }

    
    // Update is called once per frame
    void Update()
    {
        
    }
}
