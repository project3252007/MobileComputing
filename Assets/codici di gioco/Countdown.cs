using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Countdown : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI timerText;
    [SerializeField] float tempoRimanente;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(BucoNero.e == 1){
        if(tempoRimanente > 0){
            tempoRimanente -= Time.deltaTime;
        }else if(tempoRimanente < 0){
            tempoRimanente = 0;
            SceneManager.LoadScene(5);
        }

    int minuti = Mathf.FloorToInt(tempoRimanente / 60);
    int secondi = Mathf.FloorToInt(tempoRimanente % 60);
    timerText.text = string.Format("{0:00}:{1:00}", minuti, secondi);
        }
    }
}
