using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneraBase : MonoBehaviour
{
    
    public GameObject objToTP;
    public Transform tpLoc;
    private Rigidbody heldObjRb;
    void OnTriggerStay(Collider other)
    {
       
        if (other.gameObject.name == "cell") 
        {
           heldObjRb =  objToTP.GetComponent<Rigidbody>();
           heldObjRb.isKinematic = true;
            objToTP.transform.position = tpLoc.transform.position;
        }
    }
   
}
