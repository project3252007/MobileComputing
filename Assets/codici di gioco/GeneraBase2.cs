using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneraBase2 : MonoBehaviour
{
   public GameObject objToTP;
    public Transform tpLoc;
    private Rigidbody heldObjRb;
    void OnTriggerStay(Collider other)
    {
       
        if (other.gameObject.name == "ball") 
        {
           heldObjRb =  objToTP.GetComponent<Rigidbody>();
           heldObjRb.isKinematic = true;
            objToTP.transform.position = tpLoc.transform.position;
        }
    }
   
}
